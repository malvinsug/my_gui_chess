package chess.view;

import chess.model.Cell;
import chess.model.Model;

import java.awt.Color;
import java.awt.Container;
import java.beans.PropertyChangeListener;

/**
 * The main interface of the view. It gets the state it displays directly from the {@link Model}.
 */
public interface View extends PropertyChangeListener {

  /** Show the graphical user interface of the chess game. */
  void showGame();

  /** tell the view to highlight the cell. */
  void highlightTheCell(Cell cell, Color color);

  /** Reset the view of the game. */
  void setModel(Model model);
}
