package chess.view;

import static java.util.Objects.requireNonNull;

import chess.model.Cell;
import chess.model.Chess;
import chess.model.Model;
import chess.model.Pawn;
import chess.model.Phase;
import chess.model.Player;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import javax.swing.SwingUtilities;


/**
 * ChessController is a class that would be used by
 * the user to manipulate the model class, which also
 * effects the view class.
 */
public class ChessController implements Controller {

  private Model model;
  private View view;
  private Set<Cell> highlightedCell = Collections.emptySet();
  private Cell selectedCell;

  /**
   * Initiate a controller to communicate between chess model class and
   * chess view class.
   * @param chess is the chess model that we use during the game.
   */
  public ChessController(Model chess) {
    model = requireNonNull(chess);
  }

  @Override public void setView(View view) {
    this.view = requireNonNull(view);
  }

  /**
   * The game starts when this function is called.
   */
  @Override public void start() {
    view.showGame();
  }

  /**
   * When the reset button is clicked, the game should start with a fresh new game.
   */
  @Override public void resetGame() {
    model = new Chess();
    view.setModel(model);
    dispose();
    model.addPropertyChangeListener(view);
  }

  /**
   * When a piece is moved, then we should call the move
   * function in model.
   * @param from The {@link Cell source cell}.
   * @param to The {@link Cell target cell}.
   * @return
   */
  @Override public boolean move(Cell from, Cell to) {
    return model.move(from,to);
  }

  /**
   * The method removes PropertyChangeListener.
   */
  @Override
  public void dispose() {
    model.removePropertyChangeListener(view);
  }

  /**
   * Setting the new highlighted cells in GUI.
   * @param set is new highlighted cells.
   */
  @Override
  public void setHighlightedCells(Set<Cell> set) {
    this.highlightedCell = set;
  }

  /**
   * Accessing the current highlighted cells.
   * @return the current highlighted cells.
   */
  @Override
  public Set<Cell> getHighlightedCells() {
    return highlightedCell;
  }

  /**
   * Access the selected or clicked cell.
   * @return the selected or clicked cell.
   */
  @Override
  public Cell getSelectedCell() {
    return this.selectedCell;
  }

  /**
   * Setting the new selected cell.
   * @param selectedCell is the new selected cell.
   */
  @Override
  public void setSelectedCell(Cell selectedCell) {
    this.selectedCell = selectedCell;
  }

  /**
   * The MouseListener allows us to make a "button"
   * from a JPanel object. In this case it's the DrawBoard class.
   * @param cell is where the cell of chess board.
   * @param controller is helping us referencing the highlighted cell.
   * @return
   */
  @Override
  public MouseListener getMouse(Cell cell, Controller controller) {
    class PanelListener implements MouseListener {
      private Cell cell;
      private Controller controller;

      private PanelListener(Cell cell, Controller controller) {
        this.cell = cell;
        this.controller = controller;
      }

      /**
       * When the mouse is left-clicked, then it should select a
       * piece that would probably be moved.
       * The right click cancels the selected cells.
       * @param mouseEvent is the event when mouse is clicked.
       */
      @Override
      public void mouseClicked(MouseEvent mouseEvent) {
        if (model.getState().getCurrentPhase() == Phase.RUNNING) {
          Player currentPlayer = model.getState().getCurrentPlayer();
          Optional<Pawn> optPawn = model.getState().getField().get(this.cell);

          if (SwingUtilities.isRightMouseButton(mouseEvent) && mouseEvent.getClickCount() == 1) {
            unhighlightPreviousCells();
          }

          if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
            if (optPawn.isPresent() && optPawn.get().getPlayer() == currentPlayer) {
              highlightingPossibleMoves(currentPlayer, optPawn);
            } else if ((optPawn.isEmpty() || optPawn.get().getPlayer() != currentPlayer)
                    && !getHighlightedCells().contains(this.cell)) {
              unhighlightPreviousCells();
            } else if ((optPawn.isEmpty() || optPawn.get().getPlayer() != currentPlayer)
                    && getHighlightedCells().contains(this.cell)) {
              model.move(controller.getSelectedCell(), this.cell);
              unhighlightPreviousCells();
            }
          }
        }

      }

      @Override
      public void mousePressed(MouseEvent mouseEvent) {

      }

      @Override
      public void mouseReleased(MouseEvent mouseEvent) {

      }

      @Override
      public void mouseEntered(MouseEvent mouseEvent) {

      }

      @Override
      public void mouseExited(MouseEvent mouseEvent) {

      }

      /**
       * This method highlights all possible moves. If there were previously
       * highlighted moves, they should not be highlighted.
       * @param currentPlayer is the current player on the game.
       * @param optPawn is the selected pawn.
       */
      private void highlightingPossibleMoves(Player currentPlayer, Optional<Pawn> optPawn) {
        controller.setSelectedCell(this.cell);
        unhighlightPreviousCells();

        if (optPawn.isPresent() && optPawn.get().getPlayer() == currentPlayer) {
          Set<Cell> possibleMove = model.getPossibleMovesForPawn(currentPlayer, cell);
          controller.setHighlightedCells(possibleMove);
          if (possibleMove.size() > 0) {
            for (Cell value : possibleMove) {
              view.highlightTheCell(value, Color.RED);
            }
          }
        }
      }

      /**
       * This method unhighlights all the previous cells.
       */
      private void unhighlightPreviousCells() {
        if (!controller.getHighlightedCells().isEmpty()) {
          for (Cell value : controller.getHighlightedCells()) {
            view.highlightTheCell(value, Color.BLUE);
          }
          Set<Cell> emptySet = Collections.emptySet();
          controller.setHighlightedCells(emptySet);
        }
      }
    }

    PanelListener mouseListener = new PanelListener(cell,controller);
    return mouseListener;
  }

}
