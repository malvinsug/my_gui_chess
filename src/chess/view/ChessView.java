package chess.view;

import static java.util.Objects.requireNonNull;

import chess.model.Cell;
import chess.model.Model;
import chess.model.Phase;
import chess.model.Player;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import java.beans.PropertyChangeEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * ChessView controls the view of GUI in this chess game.
 */
public class ChessView extends JFrame implements View {

  private static final long serialVersionUID = 1L;
  private static final int BOARD_SIZE = 8;
  private static final int GAP_SIZE = 50;
  private static final int WINDOWS_SIZE = 800;
  private static final String CURRENT_PLAYER = "Current player: ";

  private Model model;
  private Controller controller;

  private DrawBoard[][] drawBoards = new DrawBoard[BOARD_SIZE][BOARD_SIZE];
  private MouseListener[][] listener = new MouseListener[BOARD_SIZE][BOARD_SIZE];
  private JLabel gameInfo;
  private JPanel northPanel;
  private Container container;
  private JPanel board;


  /**
   * When ChessView is instantiated, it has to take the
   * Model-object and also the Controller-object.
   * (This is MVC-Design.)
   * @param model is where the game really happens.
   * @param controller is the interface for the users.
   */
  public ChessView(Model model, Controller controller) {
    super("Pawn Game");

    //assigning inputs and setting the layout and size of the windows.
    this.model = requireNonNull(model);
    this.controller = requireNonNull(controller);
  }

  /**
   * This method shows the GUI before the first move is started.
   */
  @Override
  public void showGame() {
    settingFrame(model, controller);
  }

  /**
   * Setting up new model to the view.
   * the old board and northPanel should be first removed
   * before setting up the frame.
   * @param model is referenced by view class.
   */
  @Override
  public void setModel(Model model) {
    this.model = model;
    this.container.remove(this.board);
    this.container.remove(this.northPanel);
    settingFrame(model,controller);
  }

  /**
   * There is a notification that an attributes of
   * the model has been changed.
   * @param event is the changes of model during the game.
   */
  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(
            new Runnable() {
            @Override
            public void run() {
              handleChangeEvent(event);
            }
          });
  }

  /**
   * This method deals with changed of a state in the model class.
   * if the phase is set to finished and there is no winner, a new
   * dialog box telling the draw result would be shown. Else if there is
   * a winner, a new dialog box will congratulate the winner. After this
   * the game will be automatically restart.
   * If the phase is still running than we should notify the northPanel
   * to change the actual player.
   * @param event is an occurance that happens when an attribute of model class
   *              is changed.
   */
  private void handleChangeEvent(PropertyChangeEvent event) {
    if (event.getPropertyName().equals(Model.STATE_CHANGED)) {
      if (model.getState().getCurrentPhase() == Phase.FINISHED) {
        String[] options = {"OK"};
        if (model.getState().getWinner().isPresent()) {
          JOptionPane.showOptionDialog(null,
                  "There's a winner : Player " + model.getState().getWinner().get(),
                  "Finished",
                  JOptionPane.PLAIN_MESSAGE,
                  JOptionPane.QUESTION_MESSAGE,
                  ResourceLoader.WINNER_LOGO.get(),
                  options,
                  options[0]);
        } else if (model.getState().getWinner().isEmpty()) {
          JOptionPane.showOptionDialog(
                  null,
                  "It's a draw! Nobody wins!",
                  "Finished!",
                  JOptionPane.PLAIN_MESSAGE,
                  JOptionPane.QUESTION_MESSAGE,
                  null,
                  options,
                  options[0]);
        }
        controller.resetGame();
      } else {
        Player newPlayer = model.getState().getCurrentPlayer();
        this.gameInfo.setText(CURRENT_PLAYER + newPlayer);
        this.northPanel.add(gameInfo);
        this.container.add(northPanel, BorderLayout.NORTH);
      }
    }
  }

  /**
   * This method highlights or unhighlights the possibles move, if a piece is clicked.
   * @param cell is a possible move that should be highlighted.
   * @param color is should be red if we choose a piece, otherwise it should change the color
   *              back to blue if the second click happened
   */
  @Override
  public void highlightTheCell(Cell cell,Color color) {
    drawBoards[cell.getRow()][cell.getColumn()].setBorderColor(color);
  }

  /**
   * Setting up the panels that can changed during the game.
   * @param board is the board game of chess.
   * @param northPanel shows the game info.
   * @param southPanel shows a reset button.
   */
  private void putInteractivePanels(JPanel board, JPanel northPanel, JPanel southPanel) {
    this.container = getContentPane();
    container.add(southPanel, BorderLayout.SOUTH);
    container.add(northPanel, BorderLayout.NORTH);
    container.add(board, BorderLayout.CENTER);
  }

  /**
   * Shows the reset button in GUI.
   * @return southPanel that contains reset button.
   */
  private JPanel setResetButton() {
    JButton resetButton = new JButton("Reset");
    resetButton.addActionListener(new ResetListener());
    JPanel southPanel = new JPanel();
    southPanel.setLayout(new FlowLayout());
    southPanel.add(resetButton);
    return southPanel;
  }

  /**
   * Setting up 2 empty labels on the game.
   */
  private void setLeftRightLayout() {
    JLabel empty1 = new JLabel("");
    JLabel empty2 = new JLabel("");
    add(empty1, BorderLayout.WEST);
    add(empty2,BorderLayout.EAST);
  }

  /**
   * Setting up all GUI for the game.
   * @param model is where the logic of the games really happens.
   * @param controller is synchronizing the logic games
   *                   from model class and the view (GUI) on windows.
   */
  private void settingFrame(Model model, Controller controller) {
    setLayout(new BorderLayout(GAP_SIZE,GAP_SIZE));
    setSize(WINDOWS_SIZE,WINDOWS_SIZE);

    //setting the chess board
    board = new JPanel();
    board.setLayout(new GridLayout(8,8));

    //drawing each cell with the help from DrawBoard class.
    for (int i = 0;i < BOARD_SIZE;i++) {
      for (int j = 0;j < BOARD_SIZE;j++) {
        drawBoards[i][j] = new DrawBoard(new Cell(j,i),model);
        listener[i][j] = controller.getMouse(new Cell(j,i),this.controller);
        drawBoards[i][j].addMouseListener(listener[i][j]);
        board.add(drawBoards[i][j]);
      }
    }

    setLeftRightLayout();
    this.gameInfo = new JLabel(CURRENT_PLAYER + model.getState().getCurrentPlayer());
    this.northPanel = new JPanel();
    northPanel.setLayout(new FlowLayout());
    northPanel.setBackground(Color.WHITE);
    northPanel.add(gameInfo);
    JPanel southPanel = setResetButton();
    putInteractivePanels(board, northPanel, southPanel);

    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setVisible(true);
  }

  /**
   * ResetListener implements ActionListener which allows us
   * to give a logic when the reset button is pressed.
   */
  class ResetListener implements ActionListener {
    /**
     * Reset the game when the button is pressed.
     * @param actionEvent is the event when button is clicked.
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
      controller.resetGame();
    }
  }
}