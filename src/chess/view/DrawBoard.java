package chess.view;

import chess.model.Cell;
import chess.model.Model;
import chess.model.Pawn;
import chess.model.Player;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.JPanel;




/**
 * This is a class that helps you drawing the chess board
 * and the pawns.
 * It will draw EACH CELL on the chess board.
 */
class DrawBoard extends JPanel {

  private static final long serialVersionUID = 1L;
  private static final int BOARD_SIZE = 8;

  private Cell cell;
  private Model model;
  private Color borderColor = Color.BLUE;

  /**
   * This is the constructor for initializing the cell from chess
   * board. It will use card layout, because we want to fill all
   * the panel with checker color.
   * @param cell is the row number and the column number of the cell on the chess board.
   * @param model is the chess model that we use in in this game.
   */
  DrawBoard(Cell cell, Model model) {
    super(new CardLayout());
    this.cell = cell;
    this.model = model;
  }

  /**
   * This is where the panel is designed.
   *
   * @param g is a Graphics class, which helps you drawing the cell.
   */
  @Override
  protected void paintComponent(Graphics g) {
    drawingBorder(); // drawing color of each cell in chess board and border them

    // if i+j is a even number, than it always black square
    if ((this.cell.getRow() + this.cell.getColumn()) % 2 == 0) {
      g.setColor(Color.LIGHT_GRAY);
    } else {
      g.setColor(Color.WHITE);
    }
    g.fillRect(0, 0, getWidth(), getHeight());

    Optional<Pawn> optionalPawn = model.getState()
            .getField().get(new Cell(this.cell.getColumn(), this.cell.getRow()));
    boolean thereIsPawn = optionalPawn.isPresent();

    if (thereIsPawn) {
      Player pawnPlayer = optionalPawn.get().getPlayer();
      drawPawn(pawnPlayer, g, getWidth() / 16, 0, 0, getWidth(), getHeight());
    }
  }

  /**
   * Draw a pawn on the current selected cell.
   *
   * @param player The player that owns the cell.
   * @param g The {@link Graphics} object that allows to draw on the board.
   * @param padding Used to determine the gap-size between the cell and its border
   * @param x The coordinate marking the left point of the cell.
   * @param y The coordinate marking the upper point of the cell.
   * @param cellWidth The width of the cell.
   * @param cellHeight The height of the cell.
   */
  public void drawPawn(
          Player player, Graphics g, int padding, int x, int y, int cellWidth, int cellHeight) {

    Optional<Image> imgOpt = Optional.empty();
    switch (player) {
      case WHITE:
        g.setColor(Color.WHITE);
        imgOpt = ResourceLoader.WHITE_PAWN;
        break;
      case BLACK:
        g.setColor(Color.BLACK);
        imgOpt = ResourceLoader.BLACK_PAWN;
        break;
      default:
        throw new RuntimeException("Unhandled player: " + player);
    }

    if (imgOpt.isPresent()) {
      g.drawImage(
              imgOpt.get(),
              x + padding,
              y + padding,
              cellWidth - 2 * padding,
              cellHeight - 2 * padding,
              null);
    }
  }

  /**
   * This method draws the borderline of each cell.
   */
  private void drawingBorder() {
    if (topLeft()) {
      setBorder((BorderFactory.createMatteBorder(4, 4, 2, 2, borderColor)));
    } else if (topRight()) {
      setBorder((BorderFactory.createMatteBorder(4, 2, 2, 4, borderColor)));
    } else if (bottomLeft()) {
      setBorder((BorderFactory.createMatteBorder(2, 4, 4, 2, borderColor)));
    } else if (bottomRight()) {
      setBorder((BorderFactory.createMatteBorder(2, 2, 4, 4, borderColor)));
    } else if (leftSide()) {
      setBorder((BorderFactory.createMatteBorder(2, 4, 2, 2, borderColor)));
    } else if (rightSide()) {
      setBorder((BorderFactory.createMatteBorder(2, 2, 2, 4, borderColor)));
    } else if (topSide()) {
      setBorder((BorderFactory.createMatteBorder(4, 2, 2, 2, borderColor)));
    } else if (bottomSide()) {
      setBorder((BorderFactory.createMatteBorder(2,2, 4, 2, borderColor)));
    } else {
      setBorder((BorderFactory.createMatteBorder(2, 2, 2, 2, borderColor)));
    }
  }

  private boolean bottomSide() {
    return this.cell.getColumn() > 0
            && this.cell.getColumn() < BOARD_SIZE - 1
            && this.cell.getRow() == BOARD_SIZE - 1;
  }

  private boolean topSide() {
    return this.cell.getColumn() > 0
            && this.cell.getColumn() < BOARD_SIZE - 1
            && this.cell.getRow() == 0;
  }

  private boolean rightSide() {
    return this.cell.getRow() > 0
            && cell.getRow() < BOARD_SIZE - 1
            && this.cell.getColumn() == BOARD_SIZE - 1;
  }

  private boolean leftSide() {
    return this.cell.getRow() > 0
            && cell.getRow() < BOARD_SIZE - 1
            && this.cell.getColumn() == 0;
  }

  private boolean bottomRight() {
    return this.cell.getRow() == BOARD_SIZE - 1 && this.cell.getColumn() == BOARD_SIZE - 1;
  }

  private boolean bottomLeft() {
    return this.cell.getRow() == BOARD_SIZE - 1 && this.cell.getColumn() == 0;
  }

  private boolean topRight() {
    return this.cell.getRow() == 0 && this.cell.getColumn() == BOARD_SIZE - 1;
  }

  private boolean topLeft() {
    return this.cell.getRow() == 0 && this.cell.getColumn() == 0;
  }

  public Cell getCell() {
    return this.cell;
  }

  public void setBorderColor(Color borderColor) {
    this.borderColor = borderColor;
  }
}
