package chess.model;

import static java.util.Objects.requireNonNull;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation of a data structure class that contains all necessary attributes in order to
 * successfully play a game.
 */
public class GameState {

  private Phase currentPhase;
  private Player currentPlayer;
  private GameField gameField;
  private Player winner;

  /**
   * Constructs a new <code>GameState</code>. The state begins in a clear state, which means that
   * 1.) no pawns are on the board initially, 2.) player white is set as the beginning player, and
   * 3.) the game has its {@link Phase} set to a running state.
   */
  GameState() {
    currentPhase = Phase.RUNNING;
    currentPlayer = Player.WHITE;
    gameField = new GameField();
    winner = null;
  }

  /**
   * Return the current phase of the game.
   *
   * @return the current phase.
   */
  public Phase getCurrentPhase() {
    return currentPhase;
  }

  /**
   * Overwrites the current {@link Phase phase} of the game with the new one.
   *
   * @param phase The new phase.
   */
  void setCurrentPhase(Phase phase) {
    currentPhase = requireNonNull(phase);
  }

  /**
   * Returns the {@link GameField gamefield} that stores the data for each cell of the chess-board.
   *
   * @return the current gamefield.
   */
  public GameField getField() {
    return gameField;
  }

  /**
   * Return the player that is currently allowed to make a move.
   *
   * @return the current player
   */
  public Player getCurrentPlayer() {
    return currentPlayer;
  }

  /**
   * Return the winner of the current game. This method may only be called if the current game is
   * finished.
   *
   * @return {@link Optional#empty()} if the game's a draw. Otherwise an optional that contains the
   *     winner
   */
  public Optional<Player> getWinner() {
    if (currentPhase != Phase.FINISHED) {
      throw new IllegalStateException(
              String.format(
                      "Expected current phase to be %s, but instead it is %s",
                      Phase.FINISHED, currentPhase));
    }
    return Optional.ofNullable(winner);
  }

  /**
   * Set a winner to the current game. If a game ends in a draw, then no winner should be set here.
   *
   * @param winner An {@link Optional} containing either the winning {@link Player}, or no player if
   *     the game ends in a draw.
   */
  void setWinner(Optional<Player> winner) {
    this.winner = winner.orElse(null);
  }

  /**
   * Set the active player. For example, if {@link Player#BLACK} was previously active, the new
   * player might be set to {@link Player#WHITE}, and vice versa.
   *
   * @param newPlayer The player that may make his move now.
   */
  void setCurrentPlayer(Player newPlayer) {
    currentPlayer = newPlayer;
  }

  /**
   * Return all {@link Cell cells} of the current chess board that belong to the requested player.
   *
   * @param player The player whose cells are to be retrieved.
   * @return A set of cells on which the player has currently his pawns upon.
   */
  Set<Cell> getAllCellsOfPlayer(Player player) {
    requireNonNull(player);
    return gameField
            .getCellsOccupiedWithPawns()
            .entrySet()
            .stream()
            .filter(x -> player == x.getValue())
            .map(Entry::getKey)
            .collect(Collectors.toCollection(HashSet::new));
  }
}
