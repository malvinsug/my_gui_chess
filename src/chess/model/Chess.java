package chess.model;

import static java.util.Objects.requireNonNull;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Implementation of a chess class that handles the logic for a chess game that consists of pawns
 * only. It is the entry-class to the internal structure of the game-model and provides all methods
 * necessary for an user-interface to playing a game successfully.
 */
public class Chess implements Model {

  private static final int DIRECTION_UPWARD = 1;
  private static final int DIRECTION_DOWNWARD = -1;

  private final PropertyChangeSupport support;

  private GameState state;

  /**
   * Initialize a new Chess-Game in which everything is set up in its initial position. The game is
   * ready to be played immediately after.
   */
  public Chess() {
    support = new PropertyChangeSupport(this);
    state = new GameState();
    setInitialPawns();
  }

  /**
   * Put all pawns in their initial state on the gaming board. This is done for both {@link Player
   * players}, where {@link Player#BLACK} starts in the top row and {@link Player#WHITE} in the
   * bottom row.
   */
  private void setInitialPawns() {
    GameField field = state.getField();
    int blackRow = getBaseRowIndexOfPlayer(Player.BLACK);
    int whiteRow = getBaseRowIndexOfPlayer(Player.WHITE);

    for (int column = 0; column < GameField.SIZE; column++) {
      // The cells of both players start in the same row respectively. Only the column values
      // change in each iteration.
      Cell blackCell = new Cell(column, blackRow);
      field.set(blackCell, new Pawn(Player.BLACK));

      Cell whiteCell = new Cell(column, whiteRow);
      field.set(whiteCell, new Pawn(Player.WHITE));
    }
  }

  /**
   * Returns the row-index in which the pawns of the respective player start.
   *
   * @param player The player whose starting row is to be determined.
   * @return the row-index, ranging from 0 to 7.
   */
  private int getBaseRowIndexOfPlayer(Player player) {
    switch (player) {
      case BLACK:
        return GameField.SIZE - 1;
      case WHITE:
        return 0;
      default:
        throw new AssertionError("Unhandled player: " + player);
    }
  }

  /**
   * Add a {@link PropertyChangeListener} to the model that will be notified about the changes made
   * to the chess board.
   *
   * @param pcl the view that implements the listener.
   */
  @Override
  public void addPropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.addPropertyChangeListener(pcl);
  }

  /**
   * Remove a listener from the model, which will then no longer be notified about any events in the
   * model.
   *
   * @param pcl the view that then no longer receives notifications from the model.
   */
  @Override
  public void removePropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.removePropertyChangeListener(pcl);
  }

  /**
   * Invokes the firing of an event, such that any attached observer (i.e., {@link
   * PropertyChangeListener}) is notified that a change happened to this model.
   */
  private void notifyListeners() {
    support.firePropertyChange(STATE_CHANGED, null, this);
  }

  @Override
  public GameState getState() {
    return state;
  }

  /**
   * Move pawn from one cell to another and deal with the consequences. Moving a pawn only works if
   * their is currently a game running, if the given cell contains a pawn of the current player, and
   * if moving to the other cell is a valid chess move. After the move, it will be the turn of the
   * next player, unless he has to miss a turn because he can't move any pawn or the game is over.
   *
   * @param from cell to move pawn from
   * @param to cell to move pawn to
   * @return <code>true</code> if the move was successful, <code>false</code> otherwise
   */
  @Override
  public boolean move(Cell from, Cell to) {
    if (state.getCurrentPhase() != Phase.RUNNING) {
      return false;
    }

    GameField field = state.getField();
    Player currentPlayer = state.getCurrentPlayer();
    // Source cell must be from the current active player.
    if (!field.isWithinBounds(from)
            || !field.isWithinBounds(to)
            || !field.isCellOfPlayer(currentPlayer, from)) {
      return false;
    }

    // get all possible moves for the specific pawn
    Set<Cell> possibleMoves = getPossibleMovesForPawn(currentPlayer, from);
    if (!possibleMoves.contains(to)) {
      return false;
    }

    Pawn movedPawn = state.getField().remove(from);
    state.getField().set(to, movedPawn);

    Player nextPlayer = Player.getOpponentOf(currentPlayer);
    if (!checkForWinningCondition() && canExecuteMove(nextPlayer)) {
      state.setCurrentPlayer(nextPlayer);
    }

    notifyListeners();
    return true;
  }

  /**
   * Checks whether a winning condition is fulfilled. In that case, the {@link Phase phase} and the
   * {@link Player winning player} are set appropriately.
   *
   * @return <code>true</code> if the game is over, <code>false</code> otherwise
   */
  private boolean checkForWinningCondition() {
    if (state.getCurrentPhase() != Phase.RUNNING) {
      return true;
    }

    // check if player black has reached the base line of player white
    Set<Cell> blackPawns = state.getAllCellsOfPlayer(Player.BLACK);
    for (Cell cell : blackPawns) {
      if (cell.getRow() == getBaseRowIndexOfPlayer(Player.WHITE)) {
        state.setCurrentPhase(Phase.FINISHED);
        state.setWinner(Optional.of(Player.BLACK));
        return true;
      }
    }

    // check if player white has reached the base line of player black
    Set<Cell> whitePawns = state.getAllCellsOfPlayer(Player.WHITE);
    for (Cell cell : whitePawns) {
      if (cell.getRow() == getBaseRowIndexOfPlayer(Player.BLACK)) {
        state.setCurrentPhase(Phase.FINISHED);
        state.setWinner(Optional.of(Player.WHITE));
        return true;
      }
    }

    // check whether player black is still able to make a move
    boolean blackPawnsBlocked = !canExecuteMove(Player.BLACK);

    // check whether player white is still able to make a move
    boolean whitePawnsBlocked = !canExecuteMove(Player.WHITE);

    if (blackPawnsBlocked && whitePawnsBlocked) {
      // Both players have all of their pawns blocked and are not able to make a move anymore.
      state.setCurrentPhase(Phase.FINISHED);
      if (whitePawns.size() > blackPawns.size()) {
        state.setWinner(Optional.of(Player.WHITE));
      } else if (blackPawns.size() > whitePawns.size()) {
        state.setWinner(Optional.of(Player.BLACK));
      } else {
        assert blackPawns.size() == whitePawns.size();
        state.setWinner(Optional.empty());
      }
      return true;
    }

    return false;
  }

  /**
   * The method is checking whether the current player can execute the moves
   * or not.
   * @param player the method checking currents player's piece.
   * @return <code>true</code> if the set of possible moves is not empty,
   * <code>false</code> otherwise
   */
  private boolean canExecuteMove(Player player) {
    Set<Cell> cells = state.getAllCellsOfPlayer(player);
    for (Cell cell : cells) {
      Set<Cell> possibleMoves = getPossibleMovesForPawn(player, cell);
      if (!possibleMoves.isEmpty()) {
        // There is at least one pawn available for the player that is able to move
        return true;
      }
    }
    return false;
  }


  /**
   * Computes all possible moves for a current selected pawn. There are four possible moves in total
   * available for a single pawn, depending on the current position of the pawn as well as the
   * position of pawns from the opponent player.
   *
   * @param currentPlayer The current {@link Player player}.
   * @param cell The {@link Cell cell} that the {@link Pawn pawn} is currently positioned.
   * @return A set of cells with all possible moves for the current selected pawn.
   */
  @Override
  public Set<Cell> getPossibleMovesForPawn(Player currentPlayer, Cell cell) {
    if (currentPlayer != Player.WHITE && currentPlayer != Player.BLACK) {
      throw new IllegalArgumentException("Unhandled player: " + currentPlayer);
    }
    if (!state.getField().isCellOfPlayer(currentPlayer, cell)) {
      throw new IllegalArgumentException(
              "Cell " + cell + " does not belong to player " + currentPlayer);
    }

    Set<Cell> possibleMoves = new HashSet<>();

    GameField field = state.getField();
    int direction = currentPlayer == Player.WHITE ? DIRECTION_UPWARD : DIRECTION_DOWNWARD;

    int sourceCol = cell.getColumn();
    int sourceRow = cell.getRow();

    // check the immediate two upper (lower) neighbors of the same column
    Cell oneAhead = new Cell(sourceCol, sourceRow + direction);
    if (field.isWithinBounds(oneAhead) && field.get(oneAhead).isEmpty()) {
      possibleMoves.add(oneAhead);

      Cell twoAhead = new Cell(sourceCol, sourceRow + 2 * direction);
      if (field.isWithinBounds(twoAhead)
              && field.get(twoAhead).isEmpty()
              && sourceRow == getBaseRowIndexOfPlayer(currentPlayer)) {
        possibleMoves.add(twoAhead);
      }
    }

    Player opponent = Player.getOpponentOf(currentPlayer);

    // Add the forward left point if it is currently owned by the opponent player
    Cell diagonallyForwardLeft = new Cell(sourceCol - direction, sourceRow + direction);
    if (state.getField().isWithinBounds(diagonallyForwardLeft)
            && field.isCellOfPlayer(opponent, diagonallyForwardLeft)) {
      possibleMoves.add(diagonallyForwardLeft);
    }

    // Add the forward right point if it is currently owned by the opponent player
    Cell diagonallyForwardRight = new Cell(sourceCol + direction, sourceRow + direction);
    if (state.getField().isWithinBounds(diagonallyForwardRight)
            && field.isCellOfPlayer(opponent, diagonallyForwardRight)) {
      possibleMoves.add(diagonallyForwardRight);
    }

    return possibleMoves;
  }
}
